#include "game.h"


Game::Game(int m_spielerAnzahl)
{
    setPlayerSize(m_spielerAnzahl);
	conntectDatabase();
    sendMaxSpielerAnzahl();
}

bool Game::waitForPlayers()
{
    int x = 0;
    float prozent = 0;

	while(x!=spielerAnzahl)
    {
        std::cout << "suche nach Spielern" << std::endl;
        query->exec("SELECT COUNT(*) from Anmeldung");
        if(query->next())
        {
			prozent = (float(x)/float(spielerAnzahl)*100);

            x = query->value(0).toInt();
			std::cout << "Anzahl Spieler: " << query->value(0).toInt() << "/" << spielerAnzahl << std::endl;

            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        sendProzent(prozent);
    }
    write->exec("UPDATE `Global` SET ProzentGameBuilt= 100 WHERE 1");

    return true;
}
void Game::initializePlayer()
{
    query->exec("SELECT * from Anmeldung");
    while(query->next())
    {
        int wert = query->value(1).toInt();
        QString name = query->value(0).toString();

        player p(name);
		p.chips = startChips;
        p.id = wert;
        table.push_back(p);

        write->prepare("INSERT INTO `Spieler` (`SpielerName`, `SpielerID`, `HandKarte1`, `HandKarte2`, `Chips`, `bet`, `Action`, `YourTurn`) VALUES (?, ?, '###', '###', ?  , '0', '0', '0')");
        write->addBindValue(p.name);
        write->addBindValue(p.id);
        write->addBindValue(p.chips);
        write->exec();

        std::cout << "Spieler " + name.toStdString() + " wurde erstellt!" << std::endl;
    }
    std::cout << "Spieler Erstellung beendet!" << std::endl;
}

void Game::setShuffleSize(int m_shuffleSize)
{
    shuffleSize = m_shuffleSize;
}

void Game::setPlayerChips(int m_playerChips)
{
	startChips = m_playerChips;
}

void Game::setGrundEinsatz(int m_grundEinsatz)
{
	grundEinsatz = m_grundEinsatz;
}

void Game::conntectDatabase()
{
    db.setHostName("www.codingmarco.cf");
    db.setDatabaseName("poker");
    db.setUserName("pokerUser");
    db.setPassword("istjawitzig");
    if(db.open())std::cout << "Database wurde verbunden!" << std::endl;

    query = new QSqlQuery(db);
    write = new QSqlQuery(db);
}

void Game::sendPot()
{
    query->prepare("UPDATE Global SET Pot = ?");
	query->addBindValue(pot);
    query->exec();
}
void Game::grundEinsatzZahlen()
{
	for(unsigned int i = 0; i < table.size(); i++)
	{
		table.at(i).chips -= grundEinsatz;
        pot += grundEinsatz;
        sendPot();
	}
    std::cout << "Grundeinsatz wurde gezahlt!" << std::endl;
}

Card Game::getObersteKarte()
{
	Card obersteKarte = deck.cards.at(deck.cards.size()-1);
	deck.cards.pop_back();
    return obersteKarte;
}
void Game::setControllerStatus(bool status)
{
	// Chat
	if(status)
	{
		write->exec("DELETE FROM Chat WHERE 1");
		write->exec("TRUNCATE TABLE Chat;");
		write->exec("INSERT INTO Chat (MessageID, SpielerID, Message) VALUES (NULL, 111, 'InitialMessage')");
		std::cout << "Tabellen gecleared" << std::endl;
	}

    write->prepare("UPDATE Global SET ControllerOn = ?");
    write->addBindValue(status);
    write->exec();

    std::cout << "Controller ist an!" << std::endl;
}
void Game::sendMaxSpielerAnzahl()
{
    write->prepare("UPDATE Global SET MaxSpielerAnzahl = ?");
	write->addBindValue(spielerAnzahl);
    write->exec();
}

void Game::sendHandKarten()
{
	for(unsigned int i = 0; i < table.size(); i++)
    {
		write->prepare("UPDATE Spieler SET HandKarte1 = ?, HandKarte2 = ? where SpielerID = ?");
        write->addBindValue(table.at(i).handCards.at(0).name);
        write->addBindValue(table.at(i).handCards.at(1).name);
        write->addBindValue(table.at(i).id);
        write->exec();
    }
}

void Game::sendFlop()
{
    write->prepare("UPDATE Global SET Flop1 = ?,Flop2 = ?,Flop3 = ? where 1");
    write->addBindValue(mitte.at(0).name);
    write->addBindValue(mitte.at(1).name);
    write->addBindValue(mitte.at(2).name);
    write->exec();
}

void Game::sendTurn()
{
    write->prepare("UPDATE Global SET Turn = ? where 1");
    write->addBindValue(mitte.at(3).name);
    write->exec();
}


void Game::sendRiver()
{
    write->prepare("UPDATE Global SET River = ? where 1");
    write->addBindValue(mitte.at(4).name);
    write->exec();
}

void Game::createHandKarten()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        table.at(i).handCards.push_back(getObersteKarte());
        table.at(i).handCards.push_back(getObersteKarte());
    }
}

void Game::createFlop()
{
    deck.cards.pop_back();
    for(int i = 0;i<3;i++)
    {
        mitte.push_back(getObersteKarte());
    }
}

void Game::createTurn()
{
    deck.cards.pop_back();
    mitte.push_back(getObersteKarte());
}

void Game::createRiver()
{
    deck.cards.pop_back();
    mitte.push_back(getObersteKarte());
}

void Game::resetMitteCards()
{
    mitte.clear();
}

void Game::resetMainDeck()
{
    deck.reset();
    deck.shuffle(shuffleSize);
}

void Game::resetMitteTable()
{
    write->prepare("UPDATE Global SET Flop1 = '###',Flop2 = '###',Flop3 = '###', Turn = '###', River = '###'");
    write->exec();
}

void Game::resetSpielerCardTable()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        write->prepare("UPDATE Spieler SET HandKarte1 = '###', HandKarte2 = '###' where SpielerID = ?");
        write->addBindValue(table.at(i).id);
        write->exec();
    }
}

void Game::resetSpielerTable()
{
    write->exec("DELETE FROM Spieler WHERE 1");
}

void Game::resetAnmeldungTable()
{
    write->exec("DELETE FROM Anmeldung WHERE 1");
}

void Game::sendPlayerChips()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        write->prepare("UPDATE Spieler SET Chips = ? WHERE SpielerID = ?");
        write->addBindValue(table.at(i).chips);
        write->addBindValue(table.at(i).id);
        write->exec();
    }
}

void Game::sendProzent(float p)
{
    write->prepare("UPDATE Global SET ProzentGameBuilt = ?");
    write->addBindValue(p);
    write->exec();

}

void Game::setPlayerSize(int s)
{
    spielerAnzahl = s;
}

void Game::resetChatTable()
{
    write->exec("DELETE FROM Chat WHERE 1");
}

void Game::cleanActionTables()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        write->prepare("UPDATE Spieler SET Action = 0,bet = 0 WHERE SpielerID = ?");
        write->addBindValue(table.at(i).id);
        write->exec();
    }
}

void Game::resetFoldedStatus()
{
    write->exec("UPDATE Spieler SET folded = false where 1");
    for(unsigned int i = 0;i<table.size();i++)
        table.at(i).folded = false;
}

void Game::resetSpielerHandCards()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        for(unsigned int c = 0;c<2;c++)
        {
            table.at(i).handCards.pop_back();
        }
    }
}

void Game::sendConsoleMessage(QString message,QString Farbe)
{
    write->prepare("INSERT INTO `Chat`(`SpielerID`, `Message`, `Color`) VALUES (?,?,?)");
    write->addBindValue(16);
    write->addBindValue(message);
    write->addBindValue(Farbe);
    write->exec();
}

void Game::resetPot()
{
    pot = 0;
    sendPot();
}

void Game::resetToCall()
{
    write->exec("UPDATE Spieler SET toCall = 0 WHERE 1");
    for(unsigned int i = 0;i<table.size();i++)
    {
        table.at(i).toCall = 0;
    }
}

bool Game::onlyOneLeft(int id)
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        if(id != table.at(i).id && !table.at(i).folded)
        {
            return false;
        }
    }
    return true;
}

void Game::setHighestToCall(int highestToCall)
{
    for(unsigned int i = 0;i<table.size();i++)
    {
         table.at(i).toCall = highestToCall - table.at(i).call;
    }
}

void Game::sendToCall()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        write->prepare("UPDATE Spieler SET toCall = ? WHERE SpielerID = ?");
        write->addBindValue(table.at(i).toCall);
        write->addBindValue(table.at(i).id);
        write->exec();
    }
}

void Game::setNewToCall(int New)
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        table.at(i).toCall = New - table.at(i).call;
        sendToCall();
    }
}

void Game::resetPlayerCall()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        table.at(i).call = 0;
    }
}

void Game::setNewPlayerOrder()
{
    player x("x");
    for(unsigned int i = 0;i<table.size();i++)
    {
        if(i == 0)
        {
            x = table.at(i);
        }
        else
        {
            table.at(i-1) = table.at(i);
        }
    }
    table.at(table.size()-1) = x;
}

void Game::PlayerFold(int id)
{
    write->prepare("UPDATE Spieler SET folded = true WHERE SpielerID = ?");
    write->addBindValue(id);
    write->exec();

    write->prepare("UPDATE Spieler SET HandKarte1 = '###',HandKarte2 = '###' WHERE SpielerID = ?");
    write->addBindValue(id);
    write->exec();
}

void Game::clearGonePlayers()
{
    for(unsigned int i = 0;i<table.size();i++)
    {
        if(table.at(i).chips == 0)
        {
            sendConsoleMessage("Der Spieler [" + table.at(i).name + "] ist aus der Runde ausgeschieden!","blue");
            for(unsigned c = i;c<table.size();c++)
            {
                if(c != table.size()-1)
                    table.at(c) = table.at(c+1);
                else
                    table.pop_back();
            }
        }
    }
}
