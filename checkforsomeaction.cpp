#include "game.h"

bool Game::checkForSomeAction()
{
    bool firstRun = true;
    bool returnWert = true;

    do
    {
        bool alleAllIn = true;
        for(unsigned int i = 0;i<table.size();i++)
        {
            if(table.at(i).chips != 0)
                alleAllIn = false;
        }

        if(alleAllIn)
        {
            goto End;
        }

        for(unsigned int i = 0;i<table.size();i++)
        {

            int Action = 0;

            //Abbruch Bedingungen falls der Spieler nicht berechtig ist eine Action zu machen!
            if(table.at(i).folded || table.at(i).chips == 0)
            {
                continue;
            }

            if(onlyOneLeft(table.at(i).id))
            {
                theWinnerTakesItAll({table.at(i).id},true);
                returnWert = false;
                goto End;
            }

            if(!firstRun && table.at(i).toCall == 0)
            {
                goto End;
            }


            //Sage dem Spieler dass er an der Reihe ist!
            query->prepare("UPDATE Spieler SET YourTurn = true WHERE SpielerID = ?");
            query->addBindValue(table.at(i).id);
            query->exec();


            //Action Auslesen!
            query->prepare("SELECT Action FROM Spieler WHERE SpielerID = ?");
            query->addBindValue(table.at(i).id);
            query->exec();
            if(query->next())
                Action = query->value(0).toInt();

            //Action Verarbeitung!
            while(Action == 0)
            {
                //Action Auslesen!
                query->prepare("SELECT Action FROM Spieler WHERE SpielerID = ?");
                query->addBindValue(table.at(i).id);
                query->exec();
                if(query->next())
                    Action = query->value(0).toInt();

                std::this_thread::sleep_for(std::chrono::milliseconds(300));
            }

            //Fold
            if(Action == 1)
            {
                table.at(i).folded = true;
                PlayerFold(table.at(i).id);
                sendConsoleMessage("[" + table.at(i).name + "] hat gefolded!");
            }

            //Call
            else if(Action == 2)
            {
                pot += table.at(i).toCall;

                table.at(i).chips -= table.at(i).toCall;
                table.at(i).call = table.at(i).toCall;
                table.at(i).toCall = table.at(i).call - table.at(i).toCall;



                sendConsoleMessage("[" + table.at(i).name + "] hat gecalled!");
            }
            //bet
            else if(Action == 3)
            {
                int bet = 0;

                query->prepare("SELECT bet FROM Spieler WHERE SpielerID = ?");
                query->addBindValue(table.at(i).id);
                query->exec();
                if(query->next())
                    bet = query->value(0).toInt();

                pot += bet;
                table.at(i).chips -= bet;
                table.at(i).call += bet;
                table.at(i).toCall = 0;

                setNewToCall(table.at(i).call);

                sendConsoleMessage("[" + table.at(i).name + "] hat um " + QString::number(bet) + " geraised!");
            }
            else
            {
                sendConsoleMessage("Fehler!","Purple");
            }

            sendPlayerChips();
            sendPot();
            sendToCall();

            //Sagen dass der Spieler nicht mehr dran ist!
            write->exec("UPDATE Spieler SET YourTurn = false WHERE 1");
        }
        //reset der Actionen!
        write->exec("UPDATE Spieler SET Action = 0 WHERE 1");
        if(firstRun)
            firstRun = false;
    }
    while(true);

End:
    resetToCall();
    resetPlayerCall();

    return returnWert;
}
