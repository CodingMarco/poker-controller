#pragma once
#include "player.h"
#include "deck.h"
#include <vector>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QString>
#include <QVariant>
#include <QDebug>
#include <thread>
#include "benchmark.h"

class Game
{
public:
    std::vector<player> table;
	std::vector<Card> mitte;
	Deck deck;

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    QSqlQuery* query;
    QSqlQuery* write;


	int spielerAnzahl = 0;
	int pot = 0;
	int shuffleSize = 10;
	int grundEinsatz = 5;
    int startChips = 300;

	Game(int m_spielerAnzahl);

	void conntectDatabase();

	void initializePlayer();
	void grundEinsatzZahlen();

    //Sender
    void sendToCall();
    void sendPot();
	void sendMaxSpielerAnzahl();
    void sendProzent(float p);
    void sendConsoleMessage(QString message,QString Farbe = "Red");

    void createHandKarten();
    void createFlop();
    void createTurn();
    void createRiver();

    //CardSetter
    void sendHandKarten();
    void sendFlop();
    void sendTurn();
    void sendRiver();
    void sendPlayerChips();
    void PlayerFold(int id);

	// Setter
    void setHighestToCall(int highestToCall);
	void setPlayerChips(int m_playerChips = 500);
	void setGrundEinsatz(int m_grundEinsatz);
	void setShuffleSize(int m_shuffleSize);
    void setPlayerSize(int s);
    void setControllerStatus(bool status);
    void setNewToCall(int bet);
    void setNewPlayerOrder();

	// Getter
	bool waitForPlayers();
    bool onlyOneLeft(int id);
    bool moreThanOnePlayerWhoDidntFold();
	Card getObersteKarte();

    //Resetter
    void resetSpielerHandCards();
    void resetMainDeck();
    void resetMitteCards();
    void resetChatTable();

    void resetMitteTable();
    void resetSpielerCardTable();
    void resetSpielerTable();
    void resetAnmeldungTable();

    void cleanActionTables();
    void resetFoldedStatus();

    void resetToCall();
    void resetPlayerCall();

    void resetPot();

    // Game Essentials
    void checkWhoWonTheGame();
    bool checkForSomeAction();
    void theWinnerTakesItAll(std::vector<int> Gewinner,bool foldEnd = false);
    void clearGonePlayers();
};
