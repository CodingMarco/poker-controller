#pragma once
#include <string>
#include <iostream>
#include <QString>

class Card
{
public:
	int number, kind;
    QString name = "";

	Card(int m_number, int m_kind);
    QString setName();
};
