#include "deck.h"

Deck::Deck()
{
    srand((unsigned)time(NULL));
	initializeCards();
}
void Deck::shuffle(int howOften)
{
	for(int i = 0; i < howOften; i++)
    {
		for(int c = 0; c < 52; c++)
        {
			Card a(0,0);

			int r1 = 0, r2 = 0;

			while(r1 == r2)
            {
				r1 = getRandom();
				r2 = getRandom();
            }
			a = cards.at((unsigned)r1);
			cards.at((unsigned)r1) = cards.at((unsigned)r2);
			cards.at((unsigned)r2) = a;
        }
    }
	std::cout << "Deck wurde " << howOften << "x geshuffelt!" << std::endl;
}
int Deck::getRandom()
{
    int r = (rand()/(RAND_MAX/51));
    return r;
}
void Deck::reset()
{
	cards.clear();
	initializeCards();
}
void Deck::initializeCards()
{
    for(int i = 2;i<15;i++)
    {
        for(int c = 1;c<=4;c++)
        {
			Card a(i,c);
			cards.push_back(a);
        }
    }
}
