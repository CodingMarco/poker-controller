#include "game.h"

void Game::checkWhoWonTheGame()
{
    benchmark besteBM;
    std::vector<int> Gewinner;
    Gewinner.push_back(-1);

    //Benchmark wird für jeden Spieler ausgeführt
    for(unsigned int i = 0;i<table.size();i++)
    {
        table.at(i).ownBM.setMitteKarten(mitte);
        table.at(i).ownBM.setHandHandKarten(table.at(i).handCards);
        table.at(i).ownBM.b_start();
    }

    //Es wird die beste BM gesucht!
    //---------------------------------------------------------------------------------------
    for(unsigned int i = 0;i<table.size();i++)
    {
        //Wenn es einen klaren Gewinner gibt!---------------------------------------------------------
        if(Gewinner.at(0) == -1 || table.at(i).ownBM.Punktzahl > besteBM.Punktzahl)
        {
            besteBM = table.at(i).ownBM;
            Gewinner.clear();
            Gewinner.push_back(table.at(i).id);
        }

        //Advanced!--------------------------------------------------------------------------------------
        else if(table.at(i).ownBM.Punktzahl == besteBM.Punktzahl)
        {
            if(table.at(i).ownBM.compare(besteBM.besteAuswahl))
            {
                Gewinner.clear();
                Gewinner.push_back(table.at(i).id);
            }
            else
            {
                Gewinner.push_back(table.at(i).id);
            }
        }
    }
    //---------------------------------------------------------------------------------------
    theWinnerTakesItAll(Gewinner);
    for(unsigned int i = 0;i<table.size();i++)
        table.at(i).ownBM.resetBM();
}

void Game::theWinnerTakesItAll(std::vector<int> Gewinner,bool foldEnd)
{
    int win = pot/int(Gewinner.size());
    if(!foldEnd)
    {
        if(Gewinner.size()==1)
        {

            for(unsigned int i = 0;i<table.size();i++)
            {
                if(table.at(i).id == Gewinner.at(0))
                {
                    sendConsoleMessage("Gewonnen hat [" + table.at(i).name + "] mit einer Punktzahl von >> " + QString::number(table.at(i).ownBM.Punktzahl),"Green");
                    table.at(i).chips += win;
                }
            }
        }
        else
        {
            sendConsoleMessage("Der Pot wird gesplittet unter:","orange");
            while(Gewinner.size()!=0)
            {
                for(unsigned int i = 0;i<table.size();i++)
                {
                    if(table.at(i).id == Gewinner.at(Gewinner.size()-1))
                    {
                        sendConsoleMessage("-> " + table.at(i).name,"orange");
                        Gewinner.pop_back();
                        table.at(i).chips += win;
                    }
                }
            }
        }
    }
    else
    {
        for(unsigned int i = 0;i<table.size();i++)
        {
            if(table.at(i).id == Gewinner.at(0))
            {
                sendConsoleMessage("Gewonnen hat [" + table.at(i).name + "]","green");
                table.at(i).chips += win;
            }
        }
    }
}
