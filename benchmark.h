#pragma once
#include <iostream>
#include <vector>
#include "card.h"

class benchmark
{
public:

    std::vector<Card> karten;
    std::vector<Card> besteAuswahl;

    int Punktzahl = 0;

    int kicker = 0;

    void b_start();


    void setMitteKarten(std::vector<Card> M);
    void setHandHandKarten(std::vector<Card> HK);

    bool StrassenCheck(std::vector<Card> Auswahl);
    bool flushCheck(int counter);

    int getKicker();

    int getPunktzahlForAuswahl(std::vector<Card> Auswahl);

    void sort();
    void resetBM();

    //COMPARES
    bool compare(std::vector<Card> andereAuswahl);

    bool highCard_compare(std::vector<Card> andereAuswahl);
    bool pair_compare(std::vector<Card> andereAuswahl);
    bool threeOfAKind_compare(std::vector<Card> andereAuswahl);
    bool FullHouse_compare(std::vector<Card> andereAuswahl);
    bool fourOfAKind_compare(std::vector<Card> andereAuswahl);
    bool flush_compare(std::vector<Card> andereAuswahl);
    bool straight_compare(std::vector<Card> andereAuswahl);
};

