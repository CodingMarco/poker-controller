#include "benchmark.h"

void benchmark::b_start()
{
    //get jede Auswahl!
    for(unsigned int ersteNullStelle = 0;ersteNullStelle < 6;ersteNullStelle++)
    {
        for(unsigned int zweiteNullStelle = (ersteNullStelle+1);zweiteNullStelle < 7;zweiteNullStelle++)
        {
            std::vector<Card> moeglicheAuswahl;
            for(unsigned int i = 0;i<karten.size();i++)
            {
                if(i != ersteNullStelle && i != zweiteNullStelle)
                    moeglicheAuswahl.push_back(karten.at(i));
            }
            if(getPunktzahlForAuswahl(moeglicheAuswahl) >= Punktzahl)
            {
                Punktzahl = getPunktzahlForAuswahl(moeglicheAuswahl);
                besteAuswahl = moeglicheAuswahl;
            }
        }
    }
    sort();
}

void benchmark::setMitteKarten(std::vector<Card> M)
{
    for(unsigned int i = 0;i<M.size();i++)
        karten.push_back(M.at(i));
}

void benchmark::setHandHandKarten(std::vector<Card> HK)
{
    for(unsigned int i = 0;i<HK.size();i++)
        karten.push_back(HK.at(i));
}

int benchmark::getPunktzahlForAuswahl(std::vector<Card> Auswahl)
{
    int bestP = 0;
    int pair_counter = 0;
    int flush_counter = 0;

    for(unsigned int i = 0;i<Auswahl.size();i++)
    {
        for(unsigned int c = 0;c<Auswahl.size();c++)
        {
            if(c != i)
            {
                if(Auswahl.at(i).number == Auswahl.at(c).number)
                    pair_counter++;
                if(Auswahl.at(i).kind == Auswahl.at(c).kind)
                    flush_counter++;
            }
        }
    }

    //Pair
    if(pair_counter == 2 && bestP < 1)
        bestP = 1;
    //Two Pair
    if(pair_counter == 4 && bestP < 2)
        bestP = 2;
    //Drilling
    if(pair_counter == 6 && bestP < 3)
        bestP = 3;
    //Straße
    if(StrassenCheck(Auswahl) && bestP < 4)
        bestP = 4;
    //Flush
    if(flushCheck(flush_counter) && bestP < 5)
        bestP = 5;
    //FullHouse
    if(pair_counter == 8 && bestP < 6)
        bestP = 6;
    //Vierling
    if(pair_counter == 12 && bestP < 7)
        bestP = 7;
    //StraightFlush
    if(flushCheck(flush_counter) && StrassenCheck(Auswahl) && bestP < 8)
        bestP = 8;
    return bestP;
}

bool benchmark::flushCheck(int counter)
{
    if(counter == 20)
        return true;
    else
        return false;
}

bool benchmark::StrassenCheck(std::vector<Card> Auswahl)
{
    int lowest = 100;
    int highest = 0;

    for(unsigned int i = 0;i<Auswahl.size();i++)
    {
        if(Auswahl.at(i).number < lowest)
            lowest = Auswahl.at(i).number;
        else if(Auswahl.at(i).number == lowest)
            return false;
        if(Auswahl.at(i).number > highest)
            highest = Auswahl.at(i).number;
        else if(Auswahl.at(i).number == highest)
            return false;
    }
    if(highest - lowest == 4)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int benchmark::getKicker()
{
    //check ob beide HandKarten benutz wurden!
    bool ersteHandKarteBenutzt = false;
    bool zweiteHandKarteBenutz = false;

    Card HK1 = karten.at(5);
    Card HK2 = karten.at(6);

    for(unsigned int i = 0;i<besteAuswahl.size();i++)
    {
        if(HK1.name == besteAuswahl.at(i).name)
            ersteHandKarteBenutzt = true;
        if(HK2.name == besteAuswahl.at(i).name)
            ersteHandKarteBenutzt = true;
    }

    if(ersteHandKarteBenutzt && zweiteHandKarteBenutz)
        return 0;
    if(ersteHandKarteBenutzt && !zweiteHandKarteBenutz)
        return HK1.number;
    if(!ersteHandKarteBenutzt && zweiteHandKarteBenutz)
        return HK2.number;

    if(HK1.number >= HK2.number)
        return HK1.number;
    else
        return HK2.number;
}

bool benchmark::compare(std::vector<Card> andereAuswahl)
{
    if(Punktzahl == 0 || Punktzahl == 4 || Punktzahl == 8)
    {
        return highCard_compare(andereAuswahl);
    }
    else if(Punktzahl == 1 || Punktzahl == 2)
    {
        return pair_compare(andereAuswahl);
    }
    else if(Punktzahl == 3)
    {
        return threeOfAKind_compare(andereAuswahl);
    }
    else if(Punktzahl == 6)
    {
        return FullHouse_compare(andereAuswahl);
    }
    else if(Punktzahl == 7)
    {
        return fourOfAKind_compare(andereAuswahl);
    }
    else if(Punktzahl == 5)
    {
        return flush_compare(andereAuswahl);
    }
    return false;
}

bool benchmark::highCard_compare(std::vector<Card> andereAuswahl)
{
    if(andereAuswahl.at(0).number < besteAuswahl.at(0).number)
        return true;
    return false;
}

bool benchmark::pair_compare(std::vector<Card> andereAuswahl)
{
    if(andereAuswahl.at(0).number < besteAuswahl.at(0).number)
        return true;
    return false;
}

bool benchmark::threeOfAKind_compare(std::vector<Card> andereAuswahl)
{
    if(andereAuswahl.at(0).number < besteAuswahl.at(0).number)
        return true;
    return false;
}

bool benchmark::FullHouse_compare(std::vector<Card> andereAuswahl)
{
    if(andereAuswahl.at(0).number < besteAuswahl.at(0).number)
        return true;
    if(andereAuswahl.at(3).number < besteAuswahl.at(3).number)
        return true;
    return false;
}

bool benchmark::fourOfAKind_compare(std::vector<Card> andereAuswahl)
{
    if(andereAuswahl.at(0).number < besteAuswahl.at(0).number)
        return true;
    return false;
}

bool benchmark::flush_compare(std::vector<Card> andereAuswahl)
{
    for(unsigned int i = 4;i>0;i++)
    {
        if(besteAuswahl.at(i).number > andereAuswahl.at(i).number)
            return true;
    }
    return false;
}

void benchmark::sort()
{
    std::vector<Card> NeueAuswahl;

    NeueAuswahl = besteAuswahl;
    for(int z = 0;z<4;z++)
    {
        for(unsigned int i = 0;i<NeueAuswahl.size()-1;i++)
        {
            if(NeueAuswahl.at(i).number < NeueAuswahl.at(i+1).number)
            {
                Card x = NeueAuswahl.at(i);
                NeueAuswahl.at(i) = NeueAuswahl.at(i+1);
                NeueAuswahl.at(i+1) = x;
            }
        }
    }

    besteAuswahl = NeueAuswahl;
}

void benchmark::resetBM()
{
    karten.clear();
    besteAuswahl.clear();
    Punktzahl = 0;
    kicker = 0;
}

