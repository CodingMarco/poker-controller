#include "game.h"
#include <QApplication>
#include "benchmark.h"
#include "settings.h"
#include <iostream>
#include <thread>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    Settings s;
    s.readFile();

    Game game(s.TableSize);

    if(!game.db.open())
    {
        std::cout << "Keine Verbindung zur Datenbank! Controller wird geschlossen!" << std::endl;
        return 0;
    }

    game.setControllerStatus(true);
    game.setShuffleSize(s.shufflesize);
    game.setGrundEinsatz(s.Grundeinsatz);
    game.setPlayerChips();
    game.resetMainDeck();
    game.resetMitteTable();
    game.resetSpielerTable();
    game.resetAnmeldungTable();
    game.resetSpielerCardTable();
    game.resetChatTable();

    game.waitForPlayers();
    game.initializePlayer();
    game.sendPlayerChips();


    std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    for(int i = 0;i<3;i++)
    {
        game.grundEinsatzZahlen();
        game.sendPlayerChips();


//----------------------------------------------------------------------
//Manuelle Kartenverteilung und Erstellung!
//        game.mitte.push_back(Card(2,3));
//        game.mitte.push_back(Card(3,4));
//        game.mitte.push_back(Card(12,4));
//        game.mitte.push_back(Card(8,2));
//        game.mitte.push_back(Card(13,2));

//        game.table.at(0).handCards.push_back(Card(2,4));
//        game.table.at(0).handCards.push_back(Card(4,3));

//        game.table.at(1).handCards.push_back(Card(2,1));
//        game.table.at(1).handCards.push_back(Card(4,1));

//        game.sendHandKarten();
//        game.sendFlop();
//        game.sendTurn();
//        game.sendRiver();



//----------------------------------------------------------------------
//Automatische Kartenverteilung und Erstellung!
        game.createHandKarten();
        game.sendHandKarten();
        if(!game.checkForSomeAction())
            goto RundenEnde;

        game.createFlop();
        game.sendFlop();
        if(!game.checkForSomeAction())
            goto RundenEnde;

        game.createTurn();
        game.sendTurn();
        if(!game.checkForSomeAction())
            goto RundenEnde;

        game.createRiver();
        game.sendRiver();
        if(!game.checkForSomeAction())
            goto RundenEnde;
//----------------------------------------------------------------------

        //Aufdecken und vergleich und Chipsaufteilung
        game.checkWhoWonTheGame();

RundenEnde:

        std::this_thread::sleep_for(std::chrono::milliseconds(5000));

        //resets für nächste Runde!
        game.setNewPlayerOrder();
        game.resetPot();
        game.resetSpielerHandCards();
        game.resetMainDeck();
        game.resetMitteCards();
        game.resetMitteTable();
        game.resetFoldedStatus();
        game.resetSpielerCardTable();

        game.clearGonePlayers();
    }

    game.sendProzent(0);
    game.setControllerStatus(false);

	return a.exec();
}
