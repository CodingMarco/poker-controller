#include "settings.h"


void Settings::readFile()
{
    std::fstream read("Settings.txt");

    while(!read.eof())
    {   
        std::string Zeile;
        read >> Zeile;

        std::istringstream Text(Zeile);

        std::string part;

        int i = 0;
        std::string Kategorie = "";
        std::string Einstellung = "";

        //Erkennung von Kategorie und Einstellung
        while(std::getline(Text,part,'='))
        {
            if(i==0)
            {
                Kategorie = part;
                i++;
            }
            else
            {
                Einstellung = part;
            }
        }

        //Verwenden der Einstellung
        if(Kategorie == "Tablesize")
            TableSize = std::stoi(Einstellung);
        else if(Kategorie == "Grundeinsatz")
            Grundeinsatz = std::stoi(Einstellung);
        else if(Kategorie == "Shufflesize")
            shufflesize = std::stoi(Einstellung);
    }
}
